<?php
require_once "smpp.php";                     
//database variables 
    $db_ip="192.168.120.208";
	$db_port="1522";
	$db_user="palpay";
	$db_pass="PALPAY";
	
	
//database connection
  $db = '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = '.$db_ip.')(PORT = '.$db_port.'))
        (CONNECT_DATA =
        (SERVICE_NAME = PrePay_D)
        (SERVER = DEDICATED)
        )
        )';
  
  $conn = oci_connect($db_user,$db_pass,$db,'AL32UTF8');
	
	
	if($conn){
		writeLog('Connected to Database successfully ');
		
		//select list of merchent	
		$query_select = oci_parse($conn, "SELECT ID,PHONE_NO,SMS_TEXT from sms_notification where IS_SEND=0");
		oci_execute($query_select);
		
		if($query_select){

			while ($emp = oci_fetch_array($query_select, OCI_BOTH)) {
				$send_flag =send_sms( $emp['SMS_TEXT'] ,  $emp['PHONE_NO'] );
				
				//update is_send to 1 if message sent
				if($send_flag){
					writeLog('The message was sent to the number '.$emp['PHONE_NO']);
					
					$query_update = oci_parse($conn,'UPDATE sms_notification SET IS_SEND = 1 WHERE ID= '.$emp['ID']);
					$result = oci_execute($query_update);
					
					if($result){	
					writeLog('status IS_SEND updated to 1 in sms_notification for '.$emp['PHONE_NO']);
					}else{
						$e = oci_error();
						writeLog('Error while updating IS_SEND for '.$emp['PHONE_NO'].': '.$e['message']);
					}
					
				}else{
						writeLog('Error in sending sms to '.$emp['PHONE_NO']);
				}
			}
		}else{
			writeLog('error in select statment of sms_notification '.$emp['PHONE_NO']);
		}
	}else if (!$conn) {
		$e = oci_error();
		writeLog('Database Connection Error: '.$e['message']);
	}		

	function writeLog($data){
		$my_file = 'log-'.date('Y-m-d').'.txt';
		$handle = fopen('./logs/'.$my_file, 'a') or die('Cannot open file:  '.$my_file);
		$data =date('Y-m-d H:i:s').' -> '.$data. PHP_EOL;
		fwrite($handle, $data);
		
	} 

	//connect to smsc server and send text to the phone no 
	//return true if the message sent, false if error accured 
	function send_sms($sms_text , $phone_no){
				
		//smpp connection variabeles 
		$SMSC_IP         ="10.100.97.49";
		$SMSC_PORT       ="16000" ;                   
		$SMSC_ID         = "PalPay";                   
		$SMSC_password   ="P&lp@19!";             
		$Bind_Type       ="TRX ";                     
		$Bind_TON        ="5";                         
		$Bind_NPI        ="0";             
		$Source_TON      ="5";                        
		$Source_NPI      ="0";
		$Destination_TON ="1";
		$Destination_NPI ="1";
		$source_addr = "Ooredoo"; 
		
		//connect to smsc over smpp
		$tx=new SMPP($SMSC_IP ,$SMSC_PORT);  // host, port
		$tx->debug=true; 
		$tx->system_type=$Bind_Type; 
		$tx->bindTransmitter($SMSC_ID,$SMSC_password); // systemId, password
		
		$tx->sms_source_addr_npi=$Source_NPI;
		$tx->sms_source_addr_ton=$Source_TON;
		 
		$tx->sms_dest_addr_ton=$Destination_TON;
		$tx->sms_dest_addr_npi=$Destination_NPI; 
		$destination_addr = $phone_no;
		$destination_addr = $phone_no
		$flag =$tx->sendSMS($source_addr, $destination_addr,$sms_text); 
		$tx->close(); 
		unset($tx); 
		return $flag;
	}
?>