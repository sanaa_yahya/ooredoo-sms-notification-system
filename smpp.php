<?php 

class SMPP{ 
    //SMPP bind parameters 
    var $system_type="TRX"; 
    var $interface_version=0x34; 
    var $addr_ton=5; 
    var $addr_npi=0; 
    var $address_range=""; 
    //ESME transmitter parameters 
    var $sms_service_type=""; 
    var $sms_source_addr_ton=5; 
    var $sms_source_addr_npi=0; 
    var $sms_dest_addr_ton=1; 
    var $sms_dest_addr_npi=1; 
    var $sms_esm_class=0; 
    var $sms_protocol_id=0; 
    var $sms_priority_flag=0; 
    var $sms_schedule_delivery_time=""; 
    var $sms_validity_period=""; 
    var $sms_registered_delivery_flag=0; 
    var $sms_replace_if_present_flag=0; 
	var $sms_data_coding=0x08; //0;	
	var $sms_sm_default_msg_id=0; 

   
	function SMPP($host, $port){ 

        //internal parameters 
        $this->sequence_number=1; 
        $this->debug=false; 
        $this->pdu_queue=array(); 
        $this->host=$host; 
        $this->port=$port; 
        $this->state="closed"; 
        //open the socket 
        $this->socket=fsockopen($this->host, $this->port, $errno, $errstr, 30); 
        if($this->socket)$this->state="open"; 
    } 
	
    function sendSMS($from, $to, $message){ 
        if (strlen($from)>20 || strlen($to)>20 || strlen($message)>160)return false; 
        if($this->state!="bind_tx")return false; 
        $short_message = $message;
		 $message=mb_convert_encoding($message, 'UCS-2', 'auto');
		 
        $pdu = pack('a1cca'.(strlen($from)+1).'cca'.(strlen($to)+1).'ccca1a1ccccca'.(strlen($message)+1), 
            $this->sms_service_type, 
            $this->sms_source_addr_ton, 
            $this->sms_source_addr_npi, 
            $from,//source_addr 
            $this->sms_dest_addr_ton, 
            $this->sms_dest_addr_npi, 
            $to,//destination_addr 
            $this->sms_esm_class, 
            $this->sms_protocol_id, 
            $this->sms_priority_flag, 
            $this->sms_schedule_delivery_time, 
            $this->sms_validity_period, 
            $this->sms_registered_delivery_flag, 
            $this->sms_replace_if_present_flag, 
            $this->sms_data_coding, 
            $this->sms_sm_default_msg_id,
            strlen($message),			
			$message//short_message 
			
        ); 
        $status=$this->sendCommand(0x00000004,$pdu);         
        return ($status===0); 
    } 	

    // Sends the PDU command to the SMSC and waits for responce. 
    //return PDU status or false on error  
    function sendCommand($command_id, $pdu){ 
        if($this->state=="closed")return false; 
        $this->sendPDU($command_id, $pdu, $this->sequence_number); 
        $status=$this->readPDU_resp($this->sequence_number, $command_id); //1059
        $this->sequence_number=$this->sequence_number+1; 
        return $status; 
    } 	
	
    // Prepares and sends PDU to SMSC. 
    function sendPDU($command_id, $pdu, $seq_number){ 
        $length=strlen($pdu) + 16; 
        $header=pack("NNNN", $length, $command_id, 0, $seq_number); 
        if($this->debug){ 
            echo "Send PDU        : $length bytes\n"; 
            $this->printHex($header.$pdu); 
            echo "command_id      : ".$command_id."\n"; 
            echo "sequence number : $seq_number\n\n"; 
        } 
        $fw = fwrite($this->socket, $header.$pdu, $length); 
    } 		
	
    // Waits for SMSC responce on specific PDU
    //return PDU status or false on error 
    function readPDU_resp($seq_number, $command_id){ 
        //create responce id 
        $command_id=$command_id|0x80000000; 
        //check queue 
        for($i=0;$i<count($this->pdu_queue);$i++){ 
            $pdu=$this->pdu_queue[$i]; 
            if($pdu['sn']==$seq_number && $pdu['id']==$command_id){ 
                //remove responce 
                array_splice($this->pdu_queue, $i, 1); 
                return $pdu['status']; 
            } 
        } 
        //read pdu 
        do{ 
            $pdu=$this->readPDU(); 
            if($pdu)array_push($this->pdu_queue, $pdu); 
        }while($pdu && ($pdu['sn']!=$seq_number || $pdu['id']!=$command_id)); 
        //remove responce from queue 
        if($pdu){ 
            array_pop($this->pdu_queue); 
            return $pdu['status']; 
        } 
        return false; 
    } 	

 
     //Reads incoming PDU from SMSC. 
     //return readed PDU or false on error. 
    function readPDU(){ 
        //read PDU length 
        $tmp=fread($this->socket, 4); 
        if(!$tmp)return false; 
        extract(unpack("Nlength", $tmp)); 
        //read PDU headers 
        $tmp2=fread($this->socket, 12); 
        if(!$tmp2)return false; 
        extract(unpack("Ncommand_id/Ncommand_status/Nsequence_number", $tmp2)); 
        //read PDU body 
        if($length-16>0){ 
            $body=fread($this->socket, $length-16); 
            if(!$body)return false; 
        }else{ 
            $body=""; 
        } 
        if($this->debug){ 
            echo "Read PDU        : $length bytes\n"; 
            $this->printHex($tmp.$tmp2.$body); 
            echo "body len        : " . strlen($body) . "\n"; 
            echo "Command id      : $command_id\n"; 
            echo "Command status  : $command_status\n"; 
            echo "sequence number : $sequence_number\n\n"; 
        } 
        $pdu=array( 
            'id'=>$command_id, 
            'status'=>$command_status, 
            'sn'=>$sequence_number, 
            'body'=>$body); 

        return $pdu; 
    } 
	

     //Binds the transmitter. One object can be bound only as receiver or only as trancmitter. 
     //return true when bind was successful 
    function bindTransmitter($login, $pass){ 
        if($this->state!="open")return false; 
        if($this->debug){ 
            echo "Binding transmitter...\n\n"; 
        } 
        $status=$this->_bind($login, $pass, 0x00000002); 
        if($this->debug){ 
            echo "Binding status  : $status\n\n"; 
        } 
		
        if($status===0)$this->state="bind_tx"; 
        return ($status===0); 
    }
	
	     //Binds the socket and opens the session on SMSC 
     //return bind status or false on error 
    function _bind($login, $pass, $command_id){ 
        //make PDU 
        $pdu = pack( 
            'a'.(strlen($login)+1). 
            'a'.(strlen($pass)+1). 
            'a'.(strlen($this->system_type)+1). 
            'CCCa'.(strlen($this->address_range)+1), 
            $login, $pass, $this->system_type, 
            $this->interface_version, $this->addr_ton, 
            $this->addr_npi, $this->address_range); 
        $status=$this->sendCommand($command_id,$pdu); 
        return $status; 
    } 
	
    // Prints the binary string as hex bytes
    function printHex($pdu){ 
        $ar=unpack("C*",$pdu); 
        foreach($ar as $v){ 
            $s=dechex($v); 
            if(strlen($s)<2)$s="0$s"; 
            print "$s "; 
        } 
        print "\n"; 
		} 
		
	 //Closes the session on the SMSC server.  
    function close(){ 
        if($this->state=="closed")return; 
        if($this->debug){ 
            echo "Unbinding...\n\n"; 
        } 
        $status=$this->sendCommand(0x00000006,""); 
        if($this->debug){ 
            echo "Unbind status   : $status\n\n"; 
        } 
        fclose($this->socket); 
        $this->state="closed"; 
    } 
	} 	
   
    
 


?>